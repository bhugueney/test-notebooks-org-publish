#+TITLE: Words Embeddings
#+AUTHOR: 
#+DATE: <2023-11-08 Wed 13:30>
#+OPTIONS: toc:nil d:t
#+ipynb_options: :rise (:autolaunch true)

#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC



** Words Embeddings King - Man + Women = Queen ?
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type sublide)
  :END:


#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython :exports both :display image/png :restart
%pip install gensim
#+END_SRC

#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython :exports both :display image/png :restart
from gensim.models import KeyedVectors
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import sklearn.manifold
from sklearn.metrics import pairwise_distances
from numpy import dot
from gensim import matutils


model = KeyedVectors.load_word2vec_format('./Data/gensim-data/glove-wiki-gigaword-50/glove-wiki-gigaword-50.gz', binary=False)
most_similar=model.most_similar(positive=['king', 'woman'], negative=['man'])
words=["king", "woman", "man"]+[w for w, _ in most_similar]
vecs= [model[w] for w in words]
X=np.vstack(vecs)
def arrow(v1, v2, ax, txt=""):
    arrowprops=dict(arrowstyle='->',
                   linewidth=2,
                   shrinkA=0, shrinkB=0, color='red')
    ax.annotate(txt, v2, v1, arrowprops=arrowprops, ha='center')
pca=PCA(2)
pca.fit(X)
Z=pca.transform(X)
fig, ax = plt.subplots(1, 2,  figsize=(12, 6))
ax[0].scatter(Z[:,0], Z[:,1],alpha=0.3)
for i, txt in enumerate(words):
    ax[0].annotate(txt, (Z[i,0], Z[i,1]), ha='center')
res= model["king"]-model["man"]+model["woman"]
res_2d=pca.transform(np.vstack([res]))
ax[0].plot(res_2d[0,0], res_2d[0,1],"ro")
arrow(Z[2,:],Z[1,:],ax[0])
arrow(Z[0,:], res_2d[0,:], ax[0])
middle=(res_2d[0,:]+Z[0:])/2.0
ax[0].annotate("king - man + woman", (res_2d[0]+Z[0,:])/2.0, color="red", ha="left");
ax[0].set_title("ACP : 50→2 dimension")
def dissim(v1, v2):
    return 1.-dot(matutils.unitvec(v1), matutils.unitvec(v2))
mds=sklearn.manifold.MDS(n_components=2, n_init=1, max_iter=100, metric= True, normalized_stress='auto', dissimilarity='precomputed', random_state=0)
vecs.append(res)
Z= mds.fit_transform(pairwise_distances(vecs, vecs, dissim))
ax[1].scatter(Z[:,0], Z[:,1],alpha=0.3)
for i, txt in enumerate(words):
    ax[1].annotate(txt, (Z[i,0], Z[i,1]), ha='center')
ax[1].plot(Z[-1,0], Z[-1,1],"ro")
arrow(Z[2,:],Z[1,:],ax[1])
arrow(Z[0,:], Z[-1,:], ax[1])
middle=(Z[-1,:]+Z[0:])/2.0
ax[1].annotate("king - man + woman", (Z[-1,:]+Z[0,:])/2.0, color="red", ha="left")
ax[1].set_title("MDS : 50→2 dimensions");
#+END_SRC



