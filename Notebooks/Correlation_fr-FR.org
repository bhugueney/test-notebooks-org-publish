#+TITLE: Corrélation et Modélisation Linéaire (fr-FR)
#+AUTHOR: Bernard Hugueney
#+DATE: <2023-09-11 Mon 08:00>
#+LANGUAGE:  fr

#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC

* Corrélation
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:

[[https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Correlation_examples2.svg/1920px-Correlation_examples2.svg.png]]


* Régression linéaire
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:

- statistiques descriptives (corrélation) → modèle → statistiques inductives
- variables explicatives → variable cible (numériques)
- interpolation, extrapolation

#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
import pandas as pd
df_galton= pd.read_csv("./Data/Heights/Galton.txt", sep='\t')
#+END_SRC

#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
males=df_galton.loc[df_galton['Gender']=='M'].groupby('Family')[['Father','Mother','Height']].first().groupby('Father').first().reset_index()
#+END_SRC

#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
%pip install seaborn
#+END_SRC

#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
import seaborn as sns; sns.set_context("talk")
sns.set(style = 'white')
import matplotlib.pyplot as plt
#+END_SRC


* Corrélation →relation linéaire
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:

On peut visualiser par exemple la corrélation entre la taille d'un
échantillon d'hommes et celle de leur père :

#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
import warnings
warnings.filterwarnings('ignore')
from scipy import stats
import matplotlib.pyplot as plt
g = sns.JointGrid(x="Father", y="Height", data=df_galton[df_galton['Gender']=='M'])
g = g.plot_joint(plt.scatter, color="g", s=40, edgecolor="white")
g = g.plot_marginals(sns.distplot, kde=False, color="g")
r,p_value= stats.pearsonr(df_galton[df_galton['Gender']=='M']['Father'],df_galton[df_galton['Gender']=='M']['Height'])
g.ax_joint.text(62.5,77,f"pearson R={r:.4E}\np value={p_value:.4E}");
#+END_SRC

#+attr_ipynb: :slideshow (:slide_type subslide)
Pour plus de lisibilité on va choisir seulement 10 points :


#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
sample=males.sample(10,random_state=42)
#+END_SRC

#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
plt.scatter(df_galton[df_galton['Gender']=='M']['Father'], df_galton[df_galton['Gender']=='M']['Height'],s=4, c = 'lightgreen')
plt.scatter(sample['Father'], sample['Height'], c = 'green')
#+END_SRC


#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
sample=males.sample(10,random_state=42)
g = sns.JointGrid(x="Father", y="Height", data=sample)
g = g.plot_joint(plt.scatter, color="g", s=40, edgecolor="white")
g = g.plot_marginals(sns.distplot, kde=False, color="g")
r,p_value= stats.pearsonr(sample['Father'],sample['Height'])
g.ax_joint.text(62.5,72,f"pearson R={r:.4E}\np value={p_value:.4E}");
#+END_SRC


#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
import numpy as np
import statsmodels.api as sm
import warnings; warnings.simplefilter('ignore')
target="Height"
data = sm.add_constant(sample, prepend=False)
mod = sm.OLS(data[target],data[['Father','const']])
res=mod.fit()
res.summary()
#+END_SRC

* Modèle linéaire d'ordre 1
   :PROPERTIES:
   :ATTR_IPYNB: :slideshow (:slide_type slide)
   :END:

On peut modéliser les points par une droite :

#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
import seaborn as sns; sns.set_context("talk")
sns.set(style = 'white')
import matplotlib.pyplot as plt
ax=sns.regplot(x="Father", y=target, data=sample, order=1, ci=None,scatter_kws={'color': "blue", 'alpha': 0.3}, line_kws={'color': "green"})
#+END_SRC

** Somme des erreurs quadratiques (Sum of Squared Errors)
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:

On calcule les paramèteres de la droite dont les points sont le moins éloignés, le plus souvent en minimisant la *SSE* :

#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
import seaborn as sns; sns.set_context("talk")
sns.set(style = 'white')
import matplotlib.pyplot as plt
ax=sns.regplot(x="Father", y=target, data=sample, order=1, ci=None,scatter_kws={"color": "blue", 'alpha': 0.3}, line_kws={"color": "green"})
pred_1=res.fittedvalues.values #=res.predict(data.drop(target,axis=1)).values
sse=0
for i,t in enumerate(data.itertuples()):
    sse+=(t.Height -pred_1[i])**2
    ax.vlines(t.Father, t.Height, pred_1[i], linewidth=2,color="red")

ax.text(70, 65,
     "$sse=\\sum{{(y-\\overline{{y}})^2}}$={0:.4g}".format(sse), horizontalalignment='center', fontsize=17, color="red")
ax.text(65, 73.5,
     "$\\overline{{y}}={0:.2g} x + {1:.2g}$".format(res.params["Father"], res.params["const"]), horizontalalignment='center', fontsize=17, color="green")
plt.show()
#+END_SRC

#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
data['Father^2']=data['Father']**2
mod_2 = sm.OLS(data[target],data[['Father^2','Father','const']])
res_2=mod_2.fit()
#+END_SRC

#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
res_2.summary()
#+END_SRC

#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
def num2tex(fmt, f):
    return r'{} \times 10^{{{}}}'.format(*fmt.format(f).split("e+"))
#+END_SRC

* Modèles d'ordres supérieurs
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:

On peut toujours minimiser la /SSE/ avec un modèle d'ordre supérieur
(ici 2 et 3), mais ce n'est pas forcément une bonne idée :

#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
ax=sns.regplot(x="Father", y=target, data=sample, order=2, ci=None,scatter_kws={"color": "blue", 'alpha': 0.3}, line_kws={"color": "green"})
pred_2=res_2.fittedvalues.values #=res.predict(data.drop(target,axis=1)).values
sse=0
for i,t in enumerate(data.itertuples()):
    sse+=(t.Height -pred_2[i])**2
    ax.vlines(t.Father, t.Height, pred_2[i], linewidth=2,color="red")

ax.text(70, 65,
     "$sse=\\sum{{(y-\\overline{{y}})^2}}$={0:.4g}".format(sse), horizontalalignment='center', fontsize=17, color="red")
ax.text(67, 73,
     "$\\overline{{y}}={0:.2g} x^2 + {1:.2g} x {2}$".format(res_2.params["Father^2"],res_2.params["Father"], num2tex("{:.2g}",res_2.params["const"])), horizontalalignment='center', fontsize=15, color="green")
plt.show()
#+END_SRC

#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
data['Father^3']=data['Father']**3
mod_3 = sm.OLS(data[target],data[['Father^3','Father^2','Father','const']])
res_3=mod_3.fit()
ax=sns.regplot(x="Father", y=target, data=sample, order=3, ci=None,scatter_kws={"color": "blue", 'alpha': 0.3}, line_kws={"color": "green"})
pred_3=res_3.fittedvalues.values #=res.predict(data.drop(target,axis=1)).values
sse=0
for i,t in enumerate(data.itertuples()):
    sse+=(t.Height -pred_3[i])**2
    ax.vlines(t.Father, t.Height, pred_3[i], linewidth=2,color="red")

ax.text(70, 65,
     "$sse=\\sum{{(y-\\overline{{y}})^2}}$={0:.4g}".format(sse), horizontalalignment='center', fontsize=17, color="red")
ax.text(67, 73,
     "$\\overline{{y}}={0:.2g} x^3 + {1:.2g} x^2 + {2:.2g} x {3}$".format(res_3.params["Father^3"],res_3.params["Father^3"],res_3.params["Father"], num2tex("{:.2g}",res_3.params["const"])), horizontalalignment='center', fontsize=15, color="green")
plt.show()
#+END_SRC


* Modèles en dimensions 2 et plus
   :PROPERTIES:
   :ATTR_IPYNB: :slideshow (:slide_type slide)
   :END:


On peut aussi avoir un modèles linéaire d'ordre 1 avec 2 (ou plus)
variables explicatives. Le modèles est alors un plan (ou un
hyperplan):


#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
import numpy as np
mod2d = sm.OLS(data[target],data[['Father','Mother','const']])
res2d=mod2d.fit()
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(sample['Father'], sample['Mother'],sample['Height'])
xx, yy = np.meshgrid(range(int(min(sample['Father'])), int(max(sample['Father']))+1), range(int(min(sample['Mother'])), int(max(sample['Mother']))+1))
ax.set_xlabel('Father')
ax.set_ylabel('Mother')
ax.set_zlabel('Height');
#+END_SRC


* SSE en dimension 2
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:

On cherche alors à minimiser la somme des distances au plan :


#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(sample['Father'], sample['Mother'],sample['Height'])
xx, yy = np.meshgrid(range(int(min(sample['Father'])), int(max(sample['Father']))+1), range(int(min(sample['Mother'])), int(max(sample['Mother']))+1))
exog = pd.DataFrame({'Father': xx.ravel(), 'Mother': yy.ravel(), 'const':1.})
zz = res2d.predict(exog = exog).values.reshape(xx.shape)
ax.plot_surface(xx, yy, zz, rstride=1, cstride=1, alpha = 0.4)
pred2d= res2d.fittedvalues.values
sse=0
for i,t in enumerate(data.itertuples()):
    sse+=(t.Height -pred2d[i])**2
    ax.plot([t.Father,t.Father],[t.Mother, t.Mother], [t.Height,pred2d[i]], c='red')
ax.set_xlabel('x_1: Father')
ax.set_ylabel('x_2 : Mother')
ax.set_zlabel('y : Height')
ax.text2D(0.05, 0.95, "$\\overline{{y}}={0:.2g} x_1 + {1:.2g} x_2 + {2:.2g}$".format(res2d.params["Father"],res2d.params["Mother"],res2d.params["const"], num2tex("{:.2g}",res_3.params["const"])), horizontalalignment='center', fontsize=15, color="green", transform=ax.transAxes);
ax.text2D(1, 1,
     "$sse=\\sum{{(y-\\overline{{y}})^2}}$={0:.4g}".format(sse), horizontalalignment='center', fontsize=17, color="red", transform=ax.transAxes);
#+END_SRC

* Importance de la visualisation

De même que pour la corrélation (et pour les mêmes raisons), il est essentiel de visualiser les données et le modèle pour valider sa pertinence :


#+attr_ipynb: :hideCode t :hidePrompt t
#+BEGIN_SRC ipython
#anscombe = sns.load_dataset("anscombe")
# waiting for next seaborn realease to fix the CORS issues
anscombe= pd.read_csv("./Data/seaborn-data/anscombe.csv")
sns.lmplot(x="x", y="y", data=anscombe, col='dataset', ci=None,scatter_kws={"color": "blue", 'alpha': 0.3}, line_kws={"color": "green"});
#+END_SRC


* En pratique

On va utiliser /statsmodels/ et /scikit-learn/ pour calculer notre modèle linéaire.

D'abord, on charge le classique [[https://fr.wikipedia.org/wiki/Iris_de_Fisher][jeu de données 'iris']] qui décrit des
iris avec les mesures des pétales et des sépales et le nom de l'espèce précise :

[[https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Petal-sepal.jpg/451px-Petal-sepal.jpg]]


#+BEGIN_SRC ipython
#iris = sns.load_dataset("iris")
iris = pd.read_csv("./Data/seaborn-data/iris.csv")
iris.head()
#+END_SRC


#+BEGIN_SRC ipython
sns.pairplot(iris, hue="species");
#+END_SRC


#+BEGIN_SRC ipython
sns.lmplot(x="petal_length", y="petal_width", data=iris)
#+END_SRC


#+BEGIN_SRC ipython
sns.lmplot(x="petal_length", y="petal_width", data=iris, hue='species')
#+END_SRC


#+BEGIN_SRC ipython
sns.lmplot(x="petal_length", y="petal_width", data=iris, col='species')
#+END_SRC


#+BEGIN_SRC ipython
sns.jointplot(x='petal_length', y='petal_width', data=iris, kind='reg')
#+END_SRC


#+BEGIN_SRC ipython
sns.jointplot(x='petal_length', y='petal_width', data=iris, kind='resid')
#+END_SRC


#+BEGIN_SRC ipython
from scipy import stats
import matplotlib.pyplot as plt
g = sns.JointGrid(x="petal_length", y="petal_width", data=iris)
g = g.plot_joint(plt.scatter, color="g", s=40, edgecolor="white")
g = g.plot_marginals(sns.distplot, kde=False, color="g")
r,p_value= stats.pearsonr(iris['petal_length'],iris['petal_width'])
g.ax_joint.text(1, 2.2,f"pearson R={r:.4E}\np value={p_value:.4E}");
#+END_SRC



#+BEGIN_SRC ipython
g = sns.JointGrid(x="petal_length", y="petal_width", data=iris)
g = g.plot_joint(plt.scatter, color="g", s=40, edgecolor="white")
g = g.plot_marginals(sns.distplot, kde=False, color="g")
r,p_value= stats.pearsonr(iris['petal_length'],iris['petal_width'])
r_sq=r**2
g.ax_joint.text(1, 2.2,f"r²={r_sq:.4E}\np value={p_value:.4E}");
#+END_SRC



#+BEGIN_SRC ipython
#anscombe = sns.load_dataset("anscombe")
anscombe = pd.read_csv("./Data/seaborn-data/anscombe.csv")
#+END_SRC



#+BEGIN_SRC ipython
sns.lmplot(x="x", y="y", data=anscombe, col='dataset')
#+END_SRC


#+BEGIN_SRC ipython
def plot_anscombe(i):
    i_n=['I','II','III','IV']
    data= anscombe.query("dataset == '%s'"%i_n[i])
    g = sns.JointGrid(x="x", y="y", data= data)
    g = g.plot_joint(plt.scatter, color="g", s=80, edgecolor="white")
    g = g.plot_marginals(sns.distplot, kde=False, color="g")
    r,p_value= stats.pearsonr(data['x'], data['y'])
    g.ax_joint.text(10, 7,f"r={r:.4E}\np value={p_value:.4E}");
    return g
for i in range(4):
    plot_anscombe(i)
#+END_SRC


#+BEGIN_SRC ipython
from sklearn import linear_model
model = linear_model.LinearRegression()
results = model.fit(iris[["petal_length"]], iris['petal_width'])
print("Le meilleur modèle linéaire est : petal_width= %f + %f×petal_length" %(results.intercept_, results.coef_))
#+END_SRC


#+BEGIN_SRC ipython
import statsmodels.api as sm
model = sm.OLS(iris['petal_width'],iris[['petal_length']])# arguments order is the opposite of  sklearn.linear_model !
results = model.fit()
print(results.summary())
#+END_SRC


Le coefficient n'est pas le même que celui que nous avions avec
~sklearn.linear_model~. Ceci estd dû au fait que cette dernière avait
automatiquement ajouté un terme pour l'ordonnée à l'origine
(/intercept/) alors qu'il faut faire cela explicitement avec
~statsmodels~ :

#+BEGIN_SRC ipython
model = sm.OLS(iris['petal_width'],sm.add_constant(iris['petal_length'], prepend=False))# arguments order is the opposite of  sklearn.linear_model !
results = model.fit()
print(results.summary())
#+END_SRC

On peut afficher le résultat de la régression avec ~statsmodels~ :

#+BEGIN_SRC ipython
fig = plt.figure(figsize=(15,8))
fig = sm.graphics.plot_regress_exog(results, "petal_length", fig=fig)
#+END_SRC

Bien sûr, on peut ajouter d'autres variables explicatives :

#+BEGIN_SRC ipython
from sklearn import linear_model
model = linear_model.LinearRegression()
results = model.fit(iris[["petal_length","sepal_length"]], iris['petal_width'])
print("Le meilleur modèle linéaire est petal_width= %f + %f×petal_length + %f×sepal_length" %
      (results.intercept_, results.coef_[0], results.coef_[1]))
#+END_SRC
