{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {
        "il": null
      },
      "source": [
        "\n# Méthodes directes de résolution de systèmes, Décomposition LU\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "il": null
      },
      "source": [
        "\n## Système triangulaire\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "  Soit $n \\in \\mathbb{N}$ un entier non nul. On considère une matrice\ntriangulaire $T \\in \\mathcal{M}_n(\\mathbb{K})$ où $\\mathbb{K} = \\mathbb{R}$ ou\n$\\mathbb{K} = \\mathbb{C}$, et on cherche à résoudre le système $T X = Y$ où $X\n\\in \\mathcal{M}_{n,1}(\\mathbb{K})$ et $Y \\in \\mathcal{M}_{n,1}(\\mathbb{K})$.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "$T$ peut être une matrice triangulaire supérieure, dans ce cas, on la notera\ndans la suite $U$ pour 'up', ou bien triangulaire inférieure, et notée alors $L$\npour 'low'. Le calcul suivant est le même dans les deux cas.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Prenons le cas d'une matrice $L$. Le système à résoudre se présente ainsi :\n\n$$L = (l_{i,j})$$\n\noù\n\n$$l_{i,j} = 0, \\forall i > j$$\n\n$$\\forall i \\in ⟦ 1,\\dots,n ⟧, l_{i,j} \\neq 0$$\n\n$$\\left\\{ \\begin{array}{ll}\n l_{1,1}x_1 &=  y_1 \\\\\n l_{2,1}x_1 + l_{2,2,}x_2  &=  y_2 \\\\\n \\dots  \\\\\n l_{n,1}x_1 + \\dots + l_{n,n}n_n  &=  y_n\n\\end{array}\\right. $$\n\nNous allons compter le nombre d'opérations successives pour résoudre ce système :\n\n-   pour la première ligne :\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "$$x_1 = \\frac{y_1}{l_{1,1}}$$\n\nSoit une opération.\n\n-   pour la deuxième ligne :\n\n$$x_2 = \\frac{y_2 - l_{2,1} x_1}{l_{2,2}}$$\n\nSoit une multiplication de x<sub>1</sub> par l<sub>2,1</sub> puis une soustraction (addition) à y<sub>2</sub> puis une\ndivision par l<sub>2,2</sub> soit 3 opérations.\n\n-   à l'étape $k$ :\n    $$x_k=\\frac{y_k-\\sum^{k-1}_{i=1}{l_{k,i}x_i}}{l_{k,k}}$$\n    \n    Soit $k − 1$ multiplications et soustractions, puis une division.\n\nAu total, nous avons $\\sum_{k=1}^{n}{(1+2(k-1))}=n + n(n-1)=n^2$ opérations à effectuer.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "En calcul numérique, l'essentiel n'est pas de retenir exactement le nombre d'opérations\nà effectuer, mais l'ordre de grandeur. Les progrès techniques, les manières de programmer,\nles différences matérielles entre deux environnements d'exploitation conduisent à parler de\ncomplexité calculatoire en utilisant la notation de Landau : le grand \"$O$\". La complexité\ndu calcul retenue est donc en $O(n^2)$.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "<div class=\"alert alert-block alert-danger\">\n\nLa méthode de substitution pour résoudre un système triangulaire de taille $n$\nest en $O(n^2)$ opérations élémentaires.\n\n</div>\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "il": null
      },
      "source": [
        "\n## Vocabulaire\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "La méthode présentée sur une matrice triangulaire inférieure (*L*) porte le nom de\nméthode de substitution de Gauss, ou encore algorithme de descente. Si la matrice est\ntriangulaire supérieure (notée *U* ), on parle d'algorithme de remontée.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Les anglo-saxons parlent de \"forward substitution\" quand on utilise cette méthode\navec une matrice *L*, et de \"backward substitution\" avec une matrice *U*.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "L'unité du coût calculatoire est le flops : floating-point operations per second. Nombre\nd'opérations en virgule flottante réalisé par seconde. Un processeur moderne cadencé à\n2GHz est capable de traiter de l'ordre de 250 Gflops/s, le G désigne Giga soit 250 milliards\nd'opérations par seconde.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "il": null
      },
      "source": [
        "\n## Le point de vue du numéricien\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Le numéricien se distingue du mathématicien de par le fait qu'il doit faire\nfonctionner un algorithme sur une machine ayant une capacité limitée. Cette\ncapacité limitée se ressent selon trois problématiques différentes :\n\n1.  la vitesse d'exécution\n2.  la capacité de stockage\n3.  la précision des calculs\n\nChaque choix d'algorithme de résolution de problème est conditionné par ces trois\nécueils majeurs. Parfois bien plus vite qu'on ne le pense.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "1.  La vitesse d'exécution :\n\nPour résoudre un système de taille $(n, n)$ de la forme $AX = Y$ avec $A \\in\n\\mathcal{M}_n(\\mathbb{R}), X \\in \\mathcal{M}_{n,1}(\\mathbb{R})$ et $Y \\in\n\\mathcal{M}_{n,1}(\\mathbb{R})$, on peut être tenté d'inverser la matrice A par\nla méthode du pivot. La complexité de la méthode du pivot est en $O(n^3)$. La\nméthode de substitution décrite ci-dessus a une complexité en O(n<sup>2</sup>). Quelle est\nla différence de manière pratique? Certains problèmes industriels utilisent des\nrésolutions numériques pour des problèmes de grande taille. On peut citer les\nécoulements aérodynamiques, les fonctionnements de centrale nucléaire, les\ntraitements d'image pour l'analyse médicale, les problèmes de frottements sur\ndes objets en contact, les déformations de pièces suite à des chocs&#x2026; Sans\nparler de problèmes plus orientés recherche fondamentale comme la météorologie\nou le calcul quantique. Dans l'industrie, certains modèles de calcul pour\névaluer la déformation de pièces ou des phénomènes de contact peuvent avoir\njusqu'à 10000 points. Les valeurs de $n$ qui en découlent peuvent courament être\nde l'ordre de plusieurs milliers. Que représente le gain entre une méthode en\nO(n<sup>2</sup>) et O(n<sup>3</sup>) pour $n = 1000$ ? Et bien c'est un facteur 1000. C'est à dire\nque si une méthode prend une seconde de temps d'exécution, l'autre prendra 20\nminutes&#x2026;\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "1.  la capacité de stockage :\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "La manière de stocker les données conditionne aussi la vitesse d'exécution. Les\ndonnées d'une matrice peuvent être stockées dans des adresses contigües soit en\nparcourant les coefficients en ligne, soit en colonne. L'e\u001ecacité des méthodes\nde calcul dépendra du mode de chargement des mémoires caches. De plus, certains\nalgorithmes nécessitent de conserver des informations intermédiaires\n(récursivité), il y a toujours des compromis entre le coût de ce stockage et la\nrapidité d'exécution. En réalité, la complexité d'un algorithme est un compromis\ntemporo-spatial. Certains algorithmes optimisent l'espace mémoire en réutilisant\ndes espaces mémoires libérés par des calculs intermédiaires (le pivot par\nexemple). On parle d'algorithmes \"in place\".\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "1.  la précision des calculs.\n\nPartons d'un exemple simple : $x_0 = \\frac{1}{3},  x_{n+1}= 4x − 1$.\nNormalement, cette suite est constante et égale à 1.\n"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {},
      "execution_count": 1,
      "source": [
        "x=1/3\nfor i in range(50):\n    x=4*x-1\n    print(x)"
      ],
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "La machine n'a pas une représentation exacte de 1/3 en nombre \u001dottant. Les erreurs\nd'arrondis sont multipliées par 4 à chaque itération. Sur un problème très simple,\nla représentation finie des nombres dans un ordinateur conduit à des erreurs de\ncalcul catastrophiques.\n\nVoici un second exemple, la résolution théorique fournit une suite constante égale\nà 1:\n"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {},
      "execution_count": 1,
      "source": [
        "b=4095.1\na=1+b\nx=1\nfor i in range(10):\n    x=a*x-b\n    print(x)"
      ],
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "En très peu d'étapes, il y a une accumulation d'erreurs de calcul. Le grand\nprincipe qui en résulte est que le numéricien visera toujours à adopter un\nalgorithme de résolution qui présente le moins d'étapes de calcul possibles, et\nil essayera de calculer théoriquement l'erreur numérique liée à l'algorithme. En\nrésumé, le numéricien cherchera des algorithmes rapides, peu gourmands en\nmémoire, et dont l'efficacité calculatoire est la meilleure possible\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "il": null
      },
      "source": [
        "\n## Décomposition LU\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "il": null
      },
      "source": [
        "\n### Première étape, transformation d'une matrice en une matrice triangulaire supérieure\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Pour résoudre un système matriciel de la forme $AX = Y$ avec $A \\in\n\\mathcal{M}_n(\\mathbb{R}), X \\in \\mathcal{M}_{n,1}(\\mathbb{R})$ et $Y \\in\n\\mathcal{M}_{n,1}(\\mathbb{R})$, nous allons transformer la matrice $A$ en un\nproduit de deux matrices triangulaires : $A = LU$ , donc on sera ramené à\nappliquer deux méthodes successives de substitution :\n\n$$ AX = Y \\iff LUX = Y \\iff L(UX) = Y \\iff \\begin{cases} LZ = Y \\\\\nUX = Z \\end{cases}$$\n\navec $U X = Z$ à résoudre après avoir résolu $LZ = Y$ .\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "La transformation de AX = Y en un système triangulaire $U X = Z$ s'obtient par\napplication de la méthode du pivot. Détaillons les étapes. Posons $A = (a_{i,j})\n\\in \\mathcal{M}_n(\\mathbb{K})$. On va supposer dans un premier temps que le\npremier coefficient \"en haut à gauche\" n'est jamais nul et peut servir de pivot\nà chaque étape. Ensuite en seconde partie d'explication, on traitera le cas où\non choisit un pivot différent. On écrase en première étape les coefficients de la\npremière colonne à partir de la deuxième ligne.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "$$\\begin{pmatrix}\na_{1,1}      & a_{1,2}   & \\cdots & a_{1,n}  \\\\\na_{2,1}      & a_{2,2}   & \\cdots & a_{2,n}  \\\\\n\\vdots & \\vdots & \\ddots & \\vdots \\\\\na_{n,1}      & a_{n,2}   & \\cdots & a_{n,n}  \\\\\n\\end{pmatrix}$$\n\n$\\begin{matrix}\nL_2 & \\leftarrow & L_2 - \\frac{a_{2,1}}{a_{1,1}} L_1 \\\\\nL_3 & \\leftarrow & L_3 - \\frac{a_{3,1}}{a_{1,1}} L_1 \\\\\n \\dots \\\\\nL_n & \\leftarrow & L_n - \\frac{a_{n,1}}{a_{1,1}} L_1 \\\\\n\\end{matrix}$\n\n$$\\begin{pmatrix}\na_{1,1}      & a_{1,2}   & \\cdots & a_{1,n}  \\\\\n0      & a_{2,2}-\\frac{a_{2,1}a_{1,2}}{a_{1,1}}   & \\cdots & a_{2,n} - \\frac{a_{2,1}a_{1,n}}{a_{1,1}} \\\\\n\\vdots & \\vdots & \\ddots & \\vdots \\\\\n0      & a_{n,2} -\\frac{a_{n,1}a_{1,2}}{a_{1,1}}  & \\cdots & a_{n,n} -\\frac{a_{n,1}a_{1,n}}{a_{1,1}} \\\\\n\\end{pmatrix}$$\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Cette première étape a un coût de $2 \\times (n − 1)$ opérations (chaque\ncoefficient du sous bloc matriciel coûte deux opérations, et le sous bloc est de\ntaille $(n − 1, n − 1)$). On itère le procédé en posant :\n\n$$\\begin{pmatrix}\na_{1,1}      & a_{1,2}   & \\cdots & a_{1,n}  \\\\\n0      & a_{2,2}-\\frac{a_{2,1}a_{1,2}}{a_{1,1}}   & \\cdots & a_{2,n} - \\frac{a_{2,1}a_{1,n}}{a_{1,1}} \\\\\n\\vdots & \\vdots & \\ddots & \\vdots \\\\\n0      & a_{n,2} -\\frac{a_{n,1}a_{1,2}}{a_{1,1}}  & \\cdots & a_{n,n} -\\frac{a_{n,1}a_{1,n}}{a_{1,1}} \\\\\n\\end{pmatrix}=\\begin{pmatrix}\n a^0_{1,1} & a^0_{1,2} & \\cdots & a^0_{1,n} \\\\\n 0 & a^1_{2,2} & \\cdots & a^1{2,n} \\\\\n \\vdots & \\vdots & \\ddots & \\vdots \\\\\n 0 & a^1_{2,n} & \\cdots & a^1_{n,n} \\\\\n \\end{pmatrix}$$\n\net on applique ensuite la même technique au sous-bloc $\\begin{pmatrix}\n a^1_{2,2} & \\cdots & a^1_{2,n} \\\\\n \\vdots & \\ddots & \\vdots \\\\\n a^1_{n,2} & \\cdots & a^1_{n,n} \\\\\n \\end{pmatrix}$.\n\nCeci définit une suite de coefficients calculés de proche en proche.\n\n <div class=\"alert alert-block alert-danger\">\nLe coût global en opérations élémentaires est ainsi :\n$$\\sum_{k=1}^{n-1}{2k^2}=2 \\frac{n(n-1)(2n-1)}{6}=\\frac{2n^3}{3}+O(n^2)$$\n</div>\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "il": null
      },
      "source": [
        "\n### De la trigonalisation à la décomposition LU\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Notons $\\phi$ la suite d'opérations élémentaires appliquée à la matrice $A$ pour\nla transfor- mer en la matrice $U$ . La même suite d'opérations élémentaires\nappliquée à la matrice $I_n$ a le même effet en post-multiplication. C'est à\ndire que $\\phi(A) = U \\iff \\phi(I_n).A U$. Or $\\phi(I_nϕ)$ est une matrice\ntriangulaire inférieure (de par la nature des opérations élémentaires de pivot\nappliquées à $I_n$), et est inversible par inversion des opérations\nélémentaires. On a alors $\\phi^{-1}(\\phi(I)) = \\phi^{-1}(I_n).\\phi(I_n)=I_n$.\nD'où en notant $\\phi^{-1}(I_n)=L$ :\n\n$$A=\\phi^{-1}(I_n).U=LU$$\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "il": null
      },
      "source": [
        "\n### Un exemple\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "$$A=\\begin{pmatrix}\n1 & 4 & 7 \\\\\n2 & 5 & 8 \\\\\n3 & 6 & 10 \\\\\n\\end{pmatrix}$$\n\n$\\begin{matrix}L2 \\leftarrow L2 − 2L1 \\\\\nL3 ← L3 − 3L1 \\\\\n\\end{matrix}$\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "$A=\\begin{pmatrix}\n1 & 4 & 7 \\\\\n2 & 5 & 8 \\\\\n3 & 6 & 10 \\\\\n\\end{pmatrix}, U= \\begin{pmatrix}\n1 & 4 & 7 \\\\\n0 & -3 & -6 \\\\\n0 & -6 & -11 \\\\\n\\end{pmatrix}, \\phi(I_3)=\\begin{pmatrix}\n1 & 0 & 0 \\\\\n-2 & 1 & 0 \\\\\n-3 & 0 & 1 \\\\\n\\end{pmatrix}, \\phi^{-1}(I_3)=\\begin{pmatrix}\n1 & 0 & 0 \\\\\n2 & 1 & 0 \\\\\n3 & 0 & 1 \\\\\n\\end{pmatrix}$\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Observez qu'à chaque étape, le calcul de $\\phi^{-1]}(I_n)$ est immédiat, et ne\nnécessite que de retenir les coefficients du pivot pour les placer dans la\nposition matricielle correspondante. On recommence avec le sous bloc matriciel\n(2,2) :\n\n$L_3 \\leftarrow L_3 - 2L_1$\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "$U= \\begin{pmatrix}\n1 & 4 & 7 \\\\\n0 & -3 & -6 \\\\\n0 & 0 & 1 \\\\\n\\end{pmatrix}, \\phi(I_3)=\\begin{pmatrix}\n1 & 0 & 0 \\\\\n-2 & 1 & 0 \\\\\n-3 & -2 & 1 \\\\\n\\end{pmatrix}, \\phi^{-1}(I_3)=\\begin{pmatrix}\n1 & 0 & 0 \\\\\n2 & 1 & 0 \\\\\n3 & 2 & 1 \\\\\n\\end{pmatrix}$\n\nEt au final $A=\\begin{pmatrix}\n1 & 0 & 0 \\\\\n2 & 1 & 0 \\\\\n3 & 2 & 1\\\\\n\\end{pmatrix} \\begin{pmatrix}\n1 & 4 & 7 \\\\\n0 & -3 & -6 \\\\\n0 & 0 & 1 \\\\\n\\end{pmatrix}$\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "il": null
      },
      "source": [
        "\n### Dernière étape, permutation de lignes\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Au cours de la décomposition LU, il est possible d'obtenir une matrice dans\nlaquelle le coefficient en haut à gauche soit nul. Par exemple la méthode\nprédédemment décrite appliquée à la matrice suivante conduit à ceci :\n\n$$A=\\begin{pmatrix}\n1 & 2 & 3 \\\\\n2 & 4 & 5 \\\\\n7 & 8 & 9 \\\\\n\\end{pmatrix}, \\phi(A)=\\begin{pmatrix}\n1 & 2 & 3 \\\\\n0 & 0 & -1 \\\\\n0 & -6 & -12 \\\\\n\\end{pmatrix}$$\n\nDans ce cas, on est amené à permuter les lignes pour continuer le pivot comme\ndécrit. Mais le second membre du système $AX = Y$ doit subir les mêmes\npermutations. Il faut garder trace des permutations correspondantes. Pour cela,\nil suffit de garder une trace de l'ordre initial des lignes et d'opérer toutes\nles permutations nécessaires pour continuer la méthode. Le plus simple consiste\nà ajouter une colonne d'indexation à la matrice comme ceci :\n\n$$A=\\begin{pmatrix}\n\\begin{array}{ccc|c}\n     1 & 2 & 3 & 1\\\\\n     2 & 4 & 5 & 2 \\\\\n     7 & 8 & 9 & 3 \\\\\n\\end{array}\\end{pmatrix}$$\n\n$$\\phi(A)=\\begin{pmatrix}\n\\begin{array}{ccc|c}\n     1 & 2 & 3 & 1\\\\\n     0 & 0 & -1 & 2 \\\\\n     0 & -6 & -12 & 3 \\\\\n\\end{array}\\end{pmatrix}, \\phi^{-1}(I_3)=\\begin{pmatrix}\n1 & 0 & 0 \\\\\n2 & 1 & 0 \\\\\n7 & 0 & 1 \\\\\n\\end{pmatrix}$$\n\n$$L_2 \\leftrightarrow L_3$$\n\n$A'= \\begin{pmatrix}\n\\begin{array}{ccc|c}\n     1 & 2 & 3 & 1\\\\\n     0 & -6 & 12 & 3 \\\\\n     0 & 0 & -1 & 2 \\\\\n\\end{array}\\end{pmatrix}$\n\nIci, sur cet exemple simple, le travail est terminé, puisque la matrice est bien\ntriangulaire supérieure.\n\nAyant gardé en mémoire toutes les permutations nécessaires, on peut les\nappliquer en amont à la matrice $A$ pour que chaque pivot mis en oeuvre soit\nplacé en haut à gauche. Soit $P$ la matrice de permutation correspondante (c'est\nencore une matrice de la forme $\\phi(I_n)$ où $\\phi$ désigne la permutation\ncorrespondante appliquée aux lignes de la matrice $I_n$), on a alors :\n\n$$PAX = A'X = PY; A'=LU$$\n\nLa permutation des lignes a une autre raison essentielle d'être employée en\nraison du choix du pivot. C'est l'objet du paragraphe suivant :\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "il": null
      },
      "source": [
        "\n### Stabilité et choix optimal du pivot\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Nous allons partir d'un exemple très simple pour illustrer le problème de la\nstabilité de la méthode numérique. Soit $\\epsilon$ un nombre strictement positif\nproche de 0. et le système suivant\n\n$$A=\\begin{cases}\n\\epsilon x_1 + x_2 = 1 \\\\\nx_1 + x_2 = 2 \\\\\n\\end{cases}$$\n\nLa méthode du pivot donne $L_2 \\leftarrow L_2 - \\frac{1}{\\epsilon}L_1$ et $x_2 =\n\\frac{1-2 \\epsilon}{1- \\epsilon}$ et $x_1 = \\frac{1-x_2}{\\epsilon}$ est la\nseconde étape par remontée, une fois que x est identifié, on injecte sa valeur\ndans la première équation.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Le problème réside dans l'exécution numérique :\n"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {},
      "execution_count": 1,
      "source": [
        "epsilon=2**(-55)\nx2=(1-2*epsilon)/(1-epsilon)\nprint(x2)\nx1=(1-x2)/epsilon\nprint(x1)"
      ],
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Ce n'est clairement pas une bonne solution approchée, puisque 1 + 0 n'est pas\nproche de 2.\n\nConsidérons le même système après permutation des lignes :\n\n$A=\\begin{cases} x_1+x_2 = 2 \\\\ \\epsilon x_1 + x_2 = 1 \\\\ \\end{cases}$ C'est\névidemment le même système, donc la même résolution formelle. Mais cette fois :\n"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {},
      "execution_count": 1,
      "source": [
        "epsilon=2**(-55)\nx2=(1-2*epsilon)/(1-epsilon)\nprint(x2)\nx1=2-x2\nprint(x1)"
      ],
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "le couple (1, 1) reste une solution proche de la solution exacte.\n\n<div class=\"alert alert-block alert-danger\">\nOn retiendra que pour des raisons\nd'arrondis et de stabilité numérique, le pivot est toujours choisi comme étant\nla plus grande valeur absolue parmi la colonne des coefficients à pivoter.\n</div>\n\nIl existe une situation assez fréquente où il n'est pas nécessaire de pivoter\nles lignes, c'est à dire où les pivots sont pris systématiquement en diagonale.\nC'est lorsque la matrice est à diagonale fortement dominante. Une matrice $A$ est\nà diagonale dominante (par lignes) si $\\forall i \\in 1,\\dots ,n , |A_{i,i}|>\\sum_{i\\neq j}|A_{i,j}|$. Pour une\ntelle matrice, le pivot peut être pris en diagonale.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Exemple :\n\n$$A=\\begin{pmatrix}\n10 & 2 & 3 \\\\\n2 & 14 & 5 \\\\\n1 & 4 & 13 \\\\\n\\end{pmatrix}$$\n\nest à diagonale dominante.\n\nUn autre cas où les opérations de pivot ne produisent pas de coe\u001ecient nul en\nposition \"gauche et haute\" dans les sous blocs matriciels est lorsque la matrice\n$A$ est symétrique réelle définie positive. On appelle $k^e$ mineur fondamental\nd'une matrice $A \\in \\mathcal{M}_n(\\mathbb{R})$ le déterminant du sous bloc\ndiagonal principal de taille $(k, k)$ : $det((A_{i,j})_{i \\in ⟦1,k⟧, j \\in\n⟦1,k⟧}$. Pour une matrice définie positive, tous ses mineurs fondamentaux sont\nstrictement positifs. Cependant, pour de telles matrices, une décomposition très\nimportante est classiquement mise en oeuvre. C'est l'objet du paragraphe suivant\nqui détaille plus cette situation.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "il": null
      },
      "source": [
        "\n# Les matrices symétriques réelles et les matrices tridiagonales\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "il": null
      },
      "source": [
        "\n## les matrices symétriques réelles définies positives\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "il": null
      },
      "source": [
        "\n### Décomposition de Cholesky\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Un cas important de factorisation des matrices est celui des matrices réelles\ndéfinies positives. Soit $A \\in \\mathcal{M}_n(\\mathbb{R})$, on dit que $A$ est\nsymétrique définie positive si $A$ est symétrique réelle, donc diagonalisable,\net telle que son spectre $S_p(A)$ est inclus dans $\\mathcal{R}^{+*}$. Rappelons\nque le spectre est l'ensemble des valeurs propres de $A$. Une caractérisation\nintéressante de ces matrices est qu'elles sont exactement les matrices de Gram\nd'une famille de vecteurs libres pour un produit scalaire donné dans un espace\npréhilbertien réel.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Un exemple :\n\nOn se place dans $\\mathbb{R}^4$ et on considère les trois vecteurs suivants :\n$u= (1,1,1,1); v= (1,0 -1, 1); w=(1, 0, 0, 2)$. On peut montrer que ces trois\nvecteurs sont libres. On considère le produit scalaire usuel canonique, la\nmatrice de Gram de ces trois vecteurs est :\n\n$$G(u,v,w)= \\begin{pmatrix}\n(u|u) & (u|v) & (u|w) \\\\\n(v|u) & (v|v) & (v|w) \\\\\n(w|u) & (w|v) & (w|w) \\\\\n\\end{pmatrix}$$\n\nCe qui donne ici :\n$$G(u,v,w)= \\begin{pmatrix}\n4 & 1 & 3 \\\\\n1 & 3 & 5 \\\\\n3 & 3 & 5 \\\\\n\\end{pmatrix}$$\n\n <div class=\"alert alert-block alert-danger\">\n<b>Décomposition de Cholesky</b>\n\nSoit $A \\in \\mathcal{M}_n(\\mathbb{R})$, A symétrique et définie positive. Il\nexiste une et une seule matrice $L = (l_{i,j})$ triangulaire inférieure, telle\nque :\n\n$$\\forall i in ⟦1, \\dots, n⟧, l_{i,j} > 0 $$\net\n$$A= L.L^T$$\n\nLes coefficients de $L$ se calculent par récurrence colonne par colonne :\n\n-   colonne 1, premier coefficient (diagonal) : $l_{1,1} = \\sqrt{a_{1,1}}$\n-   colonne 1, coefficients suivants : $l_{i,1}l_{1,1}= a_{i,1}$, donc $l_{i,1}=\n        \\frac{a_{i,1}}{l_{1,1}}$\n-   colonne $i$, coefficient diagonal : $a_{i,i}= l_{i,i}^2 +\n        \\sum_{k=1}^{i-1}{l_{i,k}^2}$, donc $l_{i,i}=\\sqrt{a_{i,i} -\n        \\sum_{k=1}^{i-1}{l_{i,k}^2}}$, ce qui assure que tous les $l_{i,j}$ pour $j\n        \\leq i$ sont déterminés.\n-   colonne $i$, coefficients suivants ($j > i$) : $a_{i,j}=\n        \\sum_{r=1}^{i}{l_{j,r}l_{i,r}}$ donc $\\LARGE l_{j,i}= \\frac{a_{i,j}-\\sum_{r=1}^{i-1}{l_{j,r}l_{i,r}}}{l_{i,i}}$\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "</div>\n\nL'existence et l'unicité ne seront pas démontrées ici, elles sont une\nconséquence du théorème spectral et du procédé d'orthonormalisation de Schmidt.\nNous insisterons dans ce cours sur le procédé algorithmique de calcul des\ncoefficients.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "il": null
      },
      "source": [
        "\n### Illustration\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Cherchons la décomposition sur l'exemple proposé :\n\n$$G(u,v,w)= \\begin{pmatrix}\n4 & 1 & 3 \\\\\n1 & 3 & 3 \\\\\n3 & 3 & 5 \\\\\n\\end{pmatrix}$$\n\n$$L=\\begin{pmatrix}\nl_{1,1} & 0 & 0 \\\\\nl_{2,1} & l_{2,2} & 0 \\\\\nl_{3,1} & l_{3,2} & l_{3,3} \\\\\n\\end{pmatrix}$$\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Colonne 1 :\n\n$l_{1,1}^2=4$ donc $l_{1,1}=2$. $2l_{2,1}=1$ donc $l_{2,1}= \\frac{1}{2}$.\n$2l_{3,1}=3$ donc $l_{3,1}= \\frac{3}{2}$.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Colonne 2 :\n\n$l_{2,2}^2 + \\frac{1}{4} = 3$ donc $l_{2,2}= \\sqrt{\\frac{11}{2}}$.\n$l_{2,1}l_{3,1} + l_{2,2}l_{3,2} = 3$ donc $l_{3,2}= \\frac{9}{\\sqrt{44}}$.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Colonne 3 :\n\n$l_{3,1}^2 + l_{3,2}^2 + l_{3,3}^2 = 5$ donc $l_{3,3}= \\sqrt{\\frac{10}{11}}$.\n"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {},
      "execution_count": 1,
      "source": [
        "import numpy as np\nL=np.array([[2,0,0],[1/2,np.sqrt(11)/2,0], [3/2,9/np.sqrt(44),np.sqrt(10/11)]])\nprint(L)\nprint(np.dot(L,np.transpose(L)))"
      ],
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "il": null
      },
      "source": [
        "\n### Matrices tridiagonales et algorithme de Thomas\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Les matrices tridiagonales sont importantes, car elles apparaissent dans la\ndiscrétisation des problèmes d'équations différentielles jusqu'à l'ordre 2. On\nverra par exemple dans le chapitre sur les opérateurs de dérivation que la\nmatrice du Laplacien est tridiagonale. Soit la matrice suivante supposée\ninversible :\n\n$$ \\begin{pmatrix}\na_1 & c_1 & & 0 \\\\\nb_2 & a_2 & \\ddots & \\\\\n    & \\ddots & \\ddots & c_{n-1} \\\\\n 0  &        & b_n & a_n  \\\\\n \\end{pmatrix}$$\n\nLa décomposition $LU$ de la matrice $A$ conduit à deux matrices bidiagonales de\nla forme suivante :\n\n$$L=\\begin{pmatrix}\n1 & 0 & & 0 \\\\\n\\beta_2 & 1 & \\ddots & \\\\\n & \\ddots & \\ddots & 0 \\\\\n 0 & & \\beta_n & 1 \\\\\n \\end{pmatrix}\n U= \\begin{pmatrix}\n \\alpha_1 & c_1 & & 0 \\\\\n 0 & \\alpha_2 & \\ddots & \\\\\n  & \\ddots & \\ddots & c_{n-1} \\\\\n  0 &  & 0 & \\alpha_n \\\\\n  \\end{pmatrix}$$\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Les coefficients se calculent de proche en proche :\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "$\\alpha_1= a_1, \\beta_i = \\frac{b_i}{\\alpha_{i-1}}, \\alpha_i = a_i - \\beta_i c_{i-1}$\n\nSa complexité est en $O(n)$ car la résolution des systèmes triangulaires\ndescendant ou montant sont eux-mêmes de complexité $O(n)$.\n"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "hideCode": "true",
        "hidePrompt": "true",
        "init_cell": "true"
      },
      "execution_count": 1,
      "source": [
        "from IPython.core.display import HTML\nHTML(\"\"\"\n<style>\ndiv#notebook p, div#notebook{\n    font-size: 130%;\n    line-height: 125%;\n}.rendered_html pre, .rendered_html table{\n    font-size:130%;\n    line-height: 125%;\n}.CodeMirror, .CodeMirror pre, .CodeMirror-dialog, .CodeMirror-dialog .CodeMirror-search-field, .terminal-app .terminal {\n    font-size: 130%;\n    line-height: 125%;\n}\n</style>\n\"\"\")"
      ],
      "outputs": []
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.5.2"
    },
    "il": null
  },
  "nbformat": 4,
  "nbformat_minor": 0
}