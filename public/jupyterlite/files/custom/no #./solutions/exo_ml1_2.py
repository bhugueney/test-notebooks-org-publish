import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
lr= LogisticRegression(max_iter= 10000)
lr.fit(X, y)
from sklearn.metrics import confusion_matrix

conf_mat = confusion_matrix(y, lr.predict(X))
plt.figure(figsize=(4,3))
target_labels=["maligne","bénigne"]
sns.heatmap(conf_mat,annot = True, linewidths=0.5,fmt=".0f",xticklabels=target_labels, yticklabels=target_labels)
plt.xlabel("diagnostic modélisé")
plt.ylabel("diagnostic réel")
plt.title("Diagnostic de tumeurs");
