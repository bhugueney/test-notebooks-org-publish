from sklearn.model_selection import GridSearchCV
n_fold = 15
max_depths= range(1,35)
grid_search_decision_tree = GridSearchCV(
    estimator= DecisionTreeClassifier(),
    param_grid={'max_depth': max_depths},
    scoring='precision',
    cv=n_fold,
    refit=True,
    verbose=True
, n_jobs=-1)# autant de tâches en parallèle que possible (que de coeurs)
grid_search_decision_tree.fit(X_train, y_train)
conf_mat_grid_search_decision_tree = confusion_matrix(y_test, grid_search_decision_tree.predict(X_test))
print(conf_mat_grid_search_decision_tree)
sns.heatmap(conf_mat_grid_search_decision_tree,annot = True, linewidths=0.5,fmt=".0f",xticklabels=target_labels, yticklabels=target_labels)
plt.xlabel("diagnostic modélisé")
plt.ylabel("diagnostic réel")
plt.title("Diagnostic de tumeurs sur le jeu de test\npar arbre de décision\nhyperparamètres optimisés par grid search");
