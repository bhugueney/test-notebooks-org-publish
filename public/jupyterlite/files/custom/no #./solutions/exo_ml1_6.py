from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier().fit(X_train, y_train)
conf_mat_knn = confusion_matrix(y_test, knn.predict(X_test))
print(conf_mat_knn)
sns.heatmap(conf_mat_knn,annot = True, linewidths=0.5,fmt=".0f",xticklabels=target_labels, yticklabels=target_labels)
plt.xlabel("diagnostic modélisé")
plt.ylabel("diagnostic réel")
plt.title("Diagnostic de tumeurs sur le jeu de test\npar K plus proches voisins");
