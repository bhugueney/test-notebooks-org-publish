#+TITLE: Présentation de l'écosystème pour manipuler/visualiser des données avec Python


#+AUTHOR: Bernard HUGUENEY
#+DATE: <2022-11-29 Tue 09:00>
#+OPTIONS: toc:nil d:t
#+ipynb_options: :rise (:autolaunch true)

#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC

* Présentation de l'écosystème pour manipuler/visualiser des données avec Python
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:


* Identification et évaluation de bilbliothèques
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:

- connaître le *vocabulaire technique* (en anglais !) pour checher
- s'assurer de la compatibilité avec les autres bibliotheques utilisées (i.e. pandas)
- essayer d'estimer la pérénité :
  - disponibilité et popularité sur [[https://anaconda.org/][le dépôt anaconda]] (cf. infra)
  - historique, activité et tickets des sources (e.g. [[https://github.com/keras-team/keras][sur github]] )

Choisir le compromis risque / nouveauté pertinent.

* Principaux outils et logiciels open source pour la Data Science
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:
En plus des outils que l'on abordera, le stockage des données en base de
données, relationnelles ou non, est un point essentiel pour lequel il y a un
éventail de solutions Open Sources disponibles.
- SqLite
- DuckDB
- PostgreSQL

* Gestion de dépendances, distribiutions, dépôts → Anaconda
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:
- code que l'on écrit
- bibliothèques utilisées par le code que l'on écrit
- bibliothèques utilisées par les bibliothèques utilisées par le code que l'on écrit
- …

** Installation / gestion de dépendances : pip
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type subslide)
  :END:
 Gestionnaire historique de dépendances, installation de bibliothèques : =pip=

Bibliothèque : (nom, version), source
** Environnement virtuel : virtualenv
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type subslide)
  :END:
   Pour éviter les conflits de versions, on installe les bibliothèques dans des
environnements virtuels. Différents projets ont différentes dépendances qui
peuvent être incompatbiles
** Dépôts, Installation/ Gestion de dépendances / Environnements virtuels → Anaconda
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:

 Gestion de l'installation et des conflits de version avec [[https://anaconda.org/][Anaconda]]. Sources :
/channels/. Un outils standard [[https://docs.conda.io/projects/conda/en/stable/commands.html#conda-vs-pip-vs-virtualenv-commands][conda]] qui peut être [[https://docs.conda.io/projects/conda/en/latest/user-guide/concepts/conda-performance.html][lent]] . [[https://github.com/mamba-org/mamba][Mamba]] est une
alternative plus performante.

* Container : Docker
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:
Le concept d'environnement est pertinent mais limité à l'écosystème python.
Dépendances au sens large (système d'exploitation, bases de données, … ) → [[https://fr.wikipedia.org/wiki/Docker_(logiciel)][Docker]].

* Notebooks : Jupyter, JupyterLite / Basthon
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:

Dans un meme document interactif, on veut pouvoir présenter des explications,
des fragments de code qui manipulent des données et les résultats de ces traitements.
→ /Notebooks/.

** Jupyter
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type subslide)
  :END:
   Système client/serveur multi-langages (/kernels/) qui permet
   d'éditer/exécuter les Notebooks avec un naigateur Web. Dans une utilisation
   locale, le serveur tourne sur la même machine que le navigateur qui y accède
   par le nom /localhost/ ou par l'adresse =127.0.0.1=.

*** Hébergement de serveur
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type subslide)
  :END:
Des hébergeurs (e.g. [[https://colab.research.google.com/][Google Colab]] ou [[https://mybinder.org/][My Binder]]) permettent de faire héberger
automatiquement des serveurs de notebooks. L'installation des fichiers de
données et des dépendances est réalisée par des /containers/ (cf. supra).

*** Pseudo-serveur exécuté par le navigateur
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type subslide)
  :END:

  [[https://jupyterlite.readthedocs.io/en/latest/][JupyterLite]] s'affranchit de la nécessité d'un serveur de Notebooks au prix
  d'une compatibilité limitée à certaines bibliothèques et une perte de
  performance.

* Applications
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:
  On peut convertir les Notebooks en applications :
  - de façon cosmétique avec [[https://github.com/oschuett/appmode][Appmode]]
  - avec un serveur spécifique : [[https://github.com/voila-dashboards/voila][Voilà]]

* Initialisation d'un projet de Data Science avec Python
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:

Il faut d'abord considérer les caractéristiques du projet :
- étendue
- volume / vélocité

** Scope ?
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type subslide)
  :END:
-  évolutions : one-shot / prototype / deploiement
   écriture : une personne, équipe, turn-over

-  évolutions & collaboration →   architecture / gestion de version
   collaboration
** Sources de données
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type subslide)
  :END:
Est-ce que les données sont figées ou est-ce qu'elles doivent être mises à jour ?

** Volume / vélocité
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type subslide)
  :END:
Est-ce que les données peuvent tenir en mémoire ? Quelles contraintes de temps sur les traitements.

* Sources de données et formats de fichiers
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:
** Fichiers
*** CSV
*** JSON
*** XML
*** Parquet
** Bases de données
*** SGDBR
    - SQLite
    - DuckDB
    - PostgreSQL
*** NoSQL
** Documents
  - HTML
  - PDF
  -…
