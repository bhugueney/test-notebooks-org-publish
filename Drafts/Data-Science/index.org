#+TITLE: supports pédagogiques

#+options: toc:nil
#+html_head: <style type="text/css">
#+html_head:    div.timestamp{
#+html_head:      color: blue;
#+html_head:      font-size: 200%;
#+html_head:      font-weight: bold;
#+html_head:    }
#+html_head: </style>

* remarque préliminaires sur l'exécution de code en python :
il y a différentes façons d'exécuter du code en python :

1. avec l'interpréteur python :
  1. en lançant l'interpréteur =python= avec pour argument un fichier (généralement d'extension =.py=) contenant le programme à exécuter
  2. en lançant l'interpréteur =python= sans fichier en argument, de façon à avoir une console interactive dans laquelle on peut écrire et exécuter des instructions en python
  3. à partir d'une /environnement de développement intégré/ qui permet de cumuler les deux méthodes précédantes en permettant d'éditer des fichiers en python et de les exécuter, tout en mettant une console python à disposition.

2. avec des /notebooks/ :
  1. en local, en installant le serveur de notebooks, en plus de l'interpréteur python et des éventuelles bibliothèques, puis en s'y connectant avec un navigateur web (i.e. sur localhost)
  2. en hébergé, par exemple sur [[https://mybinder.org][mybinder]]. Une image de serveur est alors lancée, contenant tous les fichiers et avec toutes les dépendances logicielles installées. Au bout d'un certain temps, surtout lorsqu'on cesse d'interagir avec le serveur, le serveur est arrêté et tous les fichiers sont perdus ! Il faut donc penser à télécharger le Notebook s'il est modifié, et tous les fichiers générés que l'on désire conserver.
  3. en local mais sans serveur, grâce à [[https://basthon.org][Basthon]]. Le moteur d'exécution Python et toutes les dépendances sont transpilés pour être exécutés par le navigateur. Toutes les bibliothèques ne sont pas disponibles et l'on perd en performance ce qu'on gagne en facilité en évitant d'avoir à installer quoique ce soit.


** TODO Pour une utilisation locale:

  1. télécharger [[https://gitlab.com/bhugueney/test-notebooks-org-publish/-/jobs/1/artifacts/download][le dépôt]]
  2. décompresser l'archive
  3. Lancer =jupyter notebook= dans le répertoire =public= de l'arborescence récupérée
  4. cliquer sur les liens *💻* (Si vous utilisez des extensions comme [[https://fr.wikipedia.org/wiki/Privacy_Badger][Privacy Badger]], il faut les désactiver pour /localhost/ afin que l'authentification au Notebook fonctionne).


** Pour une utilisation hébergée :


Cliquer sur les liens "mybinder".

** Pour une utilisation avec Basthon :

Cliquer sur lesliens "basthon".


* Liens vers les Notebooks, mybinder et basthon :



- {{{div(2020-08-24)}}} [[https://mybinder.org/v2/gl/bhugueney%2Ftest-notebooks-org-publish/master?filepath=public/Intro-DataScience.ipynb][Data-Science : Quoi / Pourquoi / Comment ? sur mybinder]] [[https://notebook.basthon.fr/?from=https://bhugueney.gitlab.io/test-notebooks-org-publish/Intro-DataScience.ipynb&aux=https://bhugueney.gitlab.io/test-notebooks-org-publish/poeme.txt][Data-Science : Quoi / Pourquoi / Comment ? avec Bashton]]
- {{{div(2021-06-10)}}} [[https://mybinder.org/v2/gl/bhugueney%2Ftest-notebooks-org-publish/master?filepath=public/tmp.ipynb][Prep work 0 : Utilisation d'un Notebook (fr-FR) sur mybinder]] [[https://notebook.basthon.fr/?from=https://bhugueney.gitlab.io/test-notebooks-org-publish/tmp.ipynb&aux=https://bhugueney.gitlab.io/test-notebooks-org-publish/poeme.txt][Prep work 0 : Utilisation d'un Notebook (fr-FR) avec Bashton]]
- {{{div(2021-06-11)}}} [[https://mybinder.org/v2/gl/bhugueney%2Ftest-notebooks-org-publish/master?filepath=public/Prep_work_Python_FR_fr.ipynb][Prep work 1 : Les bases du language Python (fr_FR) sur mybinder]] [[https://notebook.basthon.fr/?from=https://bhugueney.gitlab.io/test-notebooks-org-publish/Prep_work_Python_FR_fr.ipynb&aux=https://bhugueney.gitlab.io/test-notebooks-org-publish/poeme.txt][Prep work 1 : Les bases du language Python (fr_FR) avec Bashton]]
- {{{div(2021-06-14)}}} [[https://mybinder.org/v2/gl/bhugueney%2Ftest-notebooks-org-publish/master?filepath=public/Prep_work_Numpy_Pandas_fr-FR.ipynb][Prep work 2 : Les bases de Numpy et Pandas (fr-FR) sur mybinder]] [[https://notebook.basthon.fr/?from=https://bhugueney.gitlab.io/test-notebooks-org-publish/Prep_work_Numpy_Pandas_fr-FR.ipynb&aux=https://bhugueney.gitlab.io/test-notebooks-org-publish/poeme.txt][Prep work 2 : Les bases de Numpy et Pandas (fr-FR) avec Bashton]]
- {{{div(2022-05-21)}}} [[https://mybinder.org/v2/gl/bhugueney%2Ftest-notebooks-org-publish/master?filepath=public/Cours_Python_Intro.ipynb][Premier cours sur Python sur mybinder]] [[https://notebook.basthon.fr/?from=https://bhugueney.gitlab.io/test-notebooks-org-publish/Cours_Python_Intro.ipynb&aux=https://bhugueney.gitlab.io/test-notebooks-org-publish/poeme.txt][Premier cours sur Python avec Bashton]]
- {{{div(2022-05-31)}}} [[https://mybinder.org/v2/gl/bhugueney%2Ftest-notebooks-org-publish/master?filepath=public/Decomposition_directe_LU.ipynb][Décomposition matricielle sur mybinder]] [[https://notebook.basthon.fr/?from=https://bhugueney.gitlab.io/test-notebooks-org-publish/Decomposition_directe_LU.ipynb&aux=https://bhugueney.gitlab.io/test-notebooks-org-publish/poeme.txt][Décomposition matricielle avec Bashton]]