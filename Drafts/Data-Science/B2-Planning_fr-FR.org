#+TITLE: B2 Planning
#+AUTHOR: Bernard HUGUENEY
#+DATE:  <2020-01-05 Sun 22:41>
#+OPTIONS: toc:nil d:t
#+ipynb_options: :rise (:autolaunch true)

#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC


* Planning de la semaine 2
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:


* J6 : Données géolocalisées
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:

** Matin : Geopandas basé sur Shapely
** Après-midi : Visualisations
   - Marques
   - KDE (Kernel Density Estimate)
   - Carte Choroplèthe

* J7 : Réducation de dimensions et apprentissage non-supervisé
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:

** Matin : Réductionn de dimensions
** Après-midi : Apprentissage non-supervisé


* J8 : Réducation de dimensions et apprentissage non-supervisé
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:

** Matin : Réseau de neurones / Perceptron Multi-couches
** Après-midi : Deep Learning
   - reconnaissance d'images
   - apprentissage par transfert

* J9 : Traitement Automatique du Langage Laturel
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:

** Matin : Représentation de textes et de mots, modèles de langage, analyse de sentiment
** Après-midi : Classification de textes, recherche de documents

* J10 : Mini projet et conclusions
  :PROPERTIES:
  :ATTR_IPYNB: :slideshow (:slide_type slide)
  :END:

** Matin : Mini Projet en autonomie
** Après-midi : Restitutions des mini projets et conclusions
