;; (load "/root/scimax/init.el")
 (let ((default-directory  "/root"))
   (normal-top-level-add-subdirs-to-load-path))
(setq-default indent-tabs-mode nil)
(add-to-list 'load-path "/root/")
(require 'ox-ipynb)


(require 'ox-publish)

(org-babel-do-load-languages 'org-babel-load-languages
                             (append org-babel-load-languages
             '((ditaa    . t)
               (dot      . t)
               (plantuml . t))))

(setq org-ditaa-jar-path "/usr/share/ditaa/ditaa.jar")
(setq org-plantuml-jar-path
      (expand-file-name "/root/plantuml.jar"))

(setq org-confirm-babel-evaluate nil)

(defun ipynb-rise-metadata (orig-func &rest args)
  (let ((data (apply orig-func args)))
    ;; now add the desired rise metadata. this is the second element
    (push '(rise . (("autolaunch" . "true"))) (cdr (second data)))
    data))

;; (advice-add 'ox-ipynb-export-to-buffer-data :around #'ipynb-rise-metadata )

;; https://lists.gnu.org/archive/html/emacs-orgmode/2019-07/msg00060.html
(setq org-export-global-macros
      '(("div" . "@@html:<div class=\"timestamp\">[$1]</div>@@")))

(defun org-sitemap-custom-entry-format (entry style project)
  "Sitemap entry format that includes date."
  (let ((filename (org-publish-find-title entry project))
  (notebookname (concat (file-name-sans-extension entry) ".ipynb")) )
    (if (= (length filename) 0)
        (format "*%s*" entry)
      (format "{{{div(%s)}}} [[%s][%s]] [[%s][%s]]"
              (format-time-string "%Y-%m-%d"
          (org-publish-find-date entry project))
        (concat "https://mybinder.org/v2/gl/bhugueney%2Ftest-notebooks-org-publish/master?urlpath=lab%2Ftree%2Fpublic%2Fjupyterlite%2Ffiles%2Fcustom%2F"
          notebookname)
        (concat filename " sur mybinder")
        (concat "https://bhugueney.gitlab.io/test-notebooks-org-publish/jupyterlite/lab/?path=custom%2F" notebookname)
              (concat filename " avec JupyterLite") ))))

(defun ox-ipynb-publish-to-org-then-notebook (plist filename pub-dir)
  "Publish an org-file to a Jupyter notebook."
  (with-current-buffer (find-file-noselect filename)
    (with-current-buffer (org-org-export-as-org)
      (let ((output (ox-ipynb-export-to-ipynb-file)))
(progn
  (print (concat "processing " filename))

  (org-publish-attachment plist (expand-file-name output)  pub-dir)

  )


  output))))

(defun ox-ipynb-publish-to-notebook (plist filename pub-dir)
  "Publish an org-file to a Jupyter notebook."
  (with-current-buffer (find-file-noselect filename)
    (let ((output (ox-ipynb-export-to-ipynb-file)))
      (org-publish-attachment plist (expand-file-name output)  pub-dir)
      output)))


(defun publish-index-as-html-otherwise-ipynb (_plist filename pub-dir)
  (if (equal (file-name-nondirectory filename)  "index.org")
      (org-html-publish-to-html _plist filename pub-dir)
    (org-ipynb-publish-to-notebook _plist filename pub-dir)))

(defun common-prefix (str1 str2)
  (common-prefix-impl str1 str2 ""))
(defun common-prefix-impl (str1 str2 res)
  (if (or (= (length str1) 0) (= (length str2) 0) (not (= (aref str1 0) (aref str2 0))))
      res
    (common-prefix-impl (substring str1 1) (substring str2 1) (concat res (substring str1 0 1)))))

(defun copy-file-creating-dirs (filename dest)
  (unless (file-exists-p (file-name-directory dest))
    (make-directory (file-name-directory dest) t))
  (copy-file filename dest t))


(defun move-with-subdirs (notebook-path tangled-path publishing-dir)
  (let ((root-path (common-prefix notebook-path tangled-path)))
    (concat publishing-dir (string-remove-prefix root-path tangled-path))))

(defun tangle-publish-with-directories (_ filename pub-dir)
  "Tangle FILENAME and place the results in PUB-DIR."
  (unless (file-exists-p pub-dir)
    (make-directory pub-dir t))
  (setq pub-dir (file-name-as-directory pub-dir))
  (mapc (lambda (el) (copy-file-creating-dirs el (move-with-subdirs filename el pub-dir))) (org-babel-tangle-file filename)))


(make-directory "./tmp" t)
(copy-directory "./Notebooks/img" "./tmp/img")
(setq index-org (concat "supports pédagogiques

#+options: toc:nil
#+html_head: <style type=\"text/css\">
#+html_head:    div.timestamp{
#+html_head:      color: blue;
#+html_head:      font-size: 200%;
#+html_head:      font-weight: bold;
#+html_head:    }
#+html_head: </style>

* remarque préliminaires sur l'exécution de code en python :
il y a différentes façons d'exécuter du code en python :

1. avec l'interpréteur python :
  1. en lançant l'interpréteur =python= avec pour argument un fichier (généralement d'extension =.py=) contenant le programme à exécuter
  2. en lançant l'interpréteur =python= sans fichier en argument, de façon à avoir une console interactive dans laquelle on peut écrire et exécuter des instructions en python
  3. à partir d'une /environnement de développement intégré/ qui permet de cumuler les deux méthodes précédantes en permettant d'éditer des fichiers en python et de les exécuter, tout en mettant une console python à disposition.

2. avec des /notebooks/ :
  1. en local, en installant le serveur de notebooks, en plus de l'interpréteur python et des éventuelles bibliothèques, puis en s'y connectant avec un navigateur web (i.e. sur [[https://localhost:8888][localhost le port par défaut étant 8888]] )
  2. en hébergé, par exemple sur [[https://mybinder.org][mybinder]]. Une image de serveur est alors lancée, contenant tous les fichiers et avec toutes les dépendances logicielles installées. Au bout d'un certain temps, surtout lorsqu'on cesse d'interagir avec le serveur, le serveur est arrêté et tous les fichiers sont perdus ! Il faut donc penser à télécharger le Notebook s'il est modifié, et tous les fichiers générés que l'on désire conserver.
  3. en local mais sans serveur, grâce à [[https://jupyterlite.readthedocs.io/en/latest/index.html][JupyterLite]]. Le moteur d'exécution Python et toutes les dépendances sont transpilés pour être exécutés par le navigateur. Toutes les bibliothèques ne sont pas disponibles et l'on perd en performance ce qu'on gagne en facilité en évitant d'avoir à installer quoique ce soit.


** TODO Pour une utilisation locale:

  1. télécharger [[https://gitlab.com/bhugueney/test-notebooks-org-publish/-/jobs/" (getenv "CI_JOB_ID") "/artifacts/download][le dépôt]]
  2. décompresser l'archive
  3. Lancer =jupyter notebook= dans le répertoire =public/jupyterlite/custom/= de l'arborescence récupérée
  4. cliquer sur les liens *💻* (Si vous utilisez des extensions comme [[https://fr.wikipedia.org/wiki/Privacy_Badger][Privacy Badger]], il faut les désactiver pour /localhost/ afin que l'authentification au Notebook fonctionne).


** Pour une utilisation hébergée :

Cliquer sur les liens \"mybinder\" pour chaque Notebook ou sur [[https://mybinder.org/v2/gl/bhugueney%2Ftest-notebooks-org-publish/master?urlpath=lab][le lien vers l'environnement global hébergé sur mybinder]].

** Pour une utilisation dans le navigateur avec JupyterLite :

Cliquer sur les liens \"JupyterLite\" pour chaque Notebook ou sur [[https://bhugueney.gitlab.io/test-notebooks-org-publish/jupyterlite/lab/][le lien vers l'environnement global exécuté dans le navigateur]].


* Liens vers les Notebooks, mybinder et JupyterLite :

"))

(setq org-publish-project-alist
      (list
        ;; ("notebooks-pre"
   ;;       :base-directory "./broken/"
   ;;       :base-extension "org"
   ;;       :publishing-directory "./Notebooks/"
   ;; :with-author nil
   ;;       :recursive t
   ;;       :publishing-function org-org-publish-to-org ;; publish-index-as-html-otherwise-ipynb
   ;;       )

  (list "notebooks"
         :base-directory "./Notebooks/"
         :base-extension "org"
         :publishing-directory "./jupyterlite/content/custom/"
   ;; :auto-index t
   ;; :index-filename "index.org"
   ;; :index-title "index"
   :auto-sitemap t
   :sitemap-filename "index.org"
   :sitemap-format-entry 'org-sitemap-custom-entry-format
   :sitemap-sort-files 'chronologically
   :sitemap-title index-org
   :with-author nil
         :recursive t
         :publishing-function 'publish-index-as-html-otherwise-ipynb
         )
  '("img"
         :base-directory "./Notebooks/img/"
         :base-extension "png\\|jpeg\\|jpg\\|gif\\|svg"
         :publishing-directory "./jupyterlite/content/custom/img"
         :recursive t
         :publishing-function org-publish-attachment
         )
  '("obipy-resources"
    :base-directory "./Notebooks/obipy-resources/"
    :base-extension "png"
    :publishing-directory "./jupyterlite/content/custom/obipy-resources"
    :recursive t
    :publishing-function org-publish-attachment
    )
 '("img-pre"
         :base-directory "./broken/img/"
         :base-extension "png\\|jpg\\|gif\\|svg"
         :publishing-directory "./public/img"
         :recursive t
         :publishing-function org-publish-attachment
         )
  '("data"
         :base-directory "./Notebooks/Data/"
         :publishing-directory "./jupyterlite/content/custom/Data"
   :base-extension any
         :recursive t
         :publishing-function org-publish-attachment
         )
  ;; hack while bashton aux url param does not allow target directory
  ;; exemple file is txt
  '("data-txt"
   :base-directory "./Notebooks/"
   :publishing-directory "./jupyterlite/content/custom/"
   :base-extension "txt"
   :recursive t
   :publishing-function org-publish-attachment
   )
  ;; ("python-src" ;; should distinguish hints and solutions ?
        ;;  :base-directory "./Notebooks"
        ;;  :base-extension "py"
        ;;  :publishing-directory "./public/"
        ;;  :recursive t
        ;;  :publishing-function org-publish-attachment
        ;;  )
  '("tangles" ;; hints and solutions
         :base-directory "./Notebooks/"
         :publishing-directory "./jupyterlite/content/custom/"
         :recursive t
         :publishing-function tangle-publish-with-directories)
  '("doc" ;; documentation
    :base-directory "./doc/"
    :publishing-directory "./jupyterlite/content/custom/doc"
    :base-extension "org"
    :recursive t
    :publishing-function org-html-publish-to-html)
  '("all" :components ( ;;"notebooks-pre"
                       "img-pre" "notebooks" "obipy-resources" "img" "data" "data-txt" "tangles"))))

(org-publish-all)
